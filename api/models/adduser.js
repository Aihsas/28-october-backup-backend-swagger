const mongoose = require('mongoose');
const Schema = mongoose.Schema;
let addUserSchema = new mongoose.Schema({
    name: { type: String },
    email: { type: String, unique: true },
    password: { type: String },
    role : { type: String},
    is_delete:{type: Boolean,default:false}
});
const adduser = mongoose.model('adduser', addUserSchema);
module.exports = adduser;