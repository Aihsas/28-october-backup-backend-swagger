const mongoose = require('mongoose');
const Schema = mongoose.Schema;
let addcartSchema = new mongoose.Schema({
    pname: { type: String },
    userId: { type: Schema.Types.ObjectId,ref: 'adduser' },
    pcost: { type: Number},
    productid:{ type: Schema.Types.ObjectId, ref: 'adminlisitng' },
    quantity : { type: Number },
    is_delete:{type: Boolean,default:false}
});
const productcart = mongoose.model('productcarts', addcartSchema);
module.exports = productcart;