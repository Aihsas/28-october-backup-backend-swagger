const mongoose = require('mongoose');
const Schema = mongoose.Schema;
let blogSchema = new mongoose.Schema({
    comment: { type: String },
    blogId: { type: Schema.Types.ObjectId,ref: 'addblog' },
    userId: { type: Schema.Types.ObjectId,ref: 'adduser' },
    rating : { type: Number},
    date :{type: Date, default: Date.now}
});
const blogcomment = mongoose.model('blogcomment', blogSchema);
module.exports = blogcomment;