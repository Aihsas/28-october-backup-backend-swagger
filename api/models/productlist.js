const mongoose = require('mongoose');
const Schema = mongoose.Schema;
let productSchema = new mongoose.Schema({
    pname: { type: String },
    pdescription: { type: String },
    pcost: { type: Number},
    status : { type: String},
    file:{type: String},
    is_delete:{type: Boolean,default:false}
});
const adminlisting = mongoose.model('adminlisitng', productSchema);
module.exports = adminlisting;