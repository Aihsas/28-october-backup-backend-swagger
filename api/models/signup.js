const mongoose = require('mongoose');
const Schema = mongoose.Schema;
let UserSchema = new mongoose.Schema({
    name: { type: String },
    email: { type: String },
    password: { type: String },
    role :{type:String},
    is_active: {type:Boolean },
    string: {type:String},
    is_delete : {type: Boolean }
});
const signup = mongoose.model('signup', UserSchema);
module.exports = signup;
