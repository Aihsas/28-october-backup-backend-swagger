const mongoose = require('mongoose');
const Schema = mongoose.Schema;
let shippingSchema = new mongoose.Schema({
    userId: { type: Schema.Types.ObjectId,ref: 'adduser' },
    name: { type: String },
    address1: { type: String },
    address2: { type: String},
    contact : { type: Number},
    city : { type: String},
    pincode : { type: Number},
    is_delete:{type: Boolean,default:false}
});
const shippingaddress = mongoose.model('shippingaddress', shippingSchema);
module.exports = shippingaddress;