var fs = require('fs');
const productModel = require('../models/productlist');
const adduserlist = require('../models/adduser');
const addblogs = require('../models/addblog');
const addusers = require('../models/adduser');
const blogcomments = require('../models/blogcomment');
const productcartModel = require('../models/addcart');
const  shippingModel = require('../models/shippingdetails');
stripe = require('stripe')('sk_test_g2A5gTTbg4lVZIzPzpeV4ZVg');
const jwt = require('jsonwebtoken')
var mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const saltRounds = 10;
module.exports = {
    createuser: createuser,
    adminLogin: adminLogin,
    product: product,
    listing: listing,
    createdelete: createdelete,
    adduser: adduser,
    adduserlisting: adduserlisting,
    deleteitem: deleteitem,
    deleteuserlist: deleteuserlist,
    editlistitem: editlistitem,
    addblog: addblog,
    bloglisting: bloglisting,
    deletebloglist: deletebloglist,
    createuserlist: createuserlist,
    addblogcomment: addblogcomment,
    getBlogBYid: getBlogBYid,
    getProductBYid:getProductBYid,
    blogcommentlisting: blogcommentlisting,
    listAddToCart:listAddToCart,
    addtocart:addtocart,
    deletecartproduct:deletecartproduct,
    shippingaddress:shippingaddress,
    generateCardToken:generateCardToken,
    paymentsucess:paymentsucess,
    edituserlist:edituserlist,
    getRoleBYid:getRoleBYid
}
var BCRYPT_SALT_ROUNDS = 12;
function createuser(req, res) {
    bcrypt.hash(req.body.password, BCRYPT_SALT_ROUNDS)
    .then(function(hashedPassword)  {
    let user = new addusers({
        name: req.body.name,
        email: req.body.email,
        password: hashedPassword,
        role: "user",
        is_active: req.body.is_active,
        is_delete: req.body.is_delete

    })
    user.save(user, function (err, response) {
        if (err) {
            res.json({
                code: 404,
                message: 'admin not Added'
            })
        } else {
            res.json({
                code: 200,
                data: response
            })
            console.log('Successfully created');

        }

    })
})
}
function createuserlist(req, res) {
    addusers.find(function (err, data) {
        res.json(data);
    })
};
function createdelete(req, res) {
    var id = req.swagger.params.id.value;
    addusers.findByIdAndRemove({ _id: id }, function (err, data) {
        if (err) { throw err; }
        else { res.json(data) }
    })

}
function adminLogin(req, res) {
    console.log(req.body)
    var email = req.body.email;
    var password = req.body.password;
    addusers.findOne({
        email: req.body.email,
        // password: req.body.password,

        // is_delete: false
    }, function (err, admin) {
        console.log("err", err);
        console.log(admin)
        if (err) {
            console.log("error");
            // res.json({
            //     code: 404,
            //     message: "error",
            //     data: null,
            // });
        }
        else if (admin) {
            bcrypt.compare(req.body.password,admin.password, function(err, passwodMatched) {
                console.log('res++++++++++',passwodMatched);
            console.log(admin, "admin");
            if(passwodMatched){
            const token = jwt.sign(admin.toJSON(), 'aihsas', {
                expiresIn: 1404
            });
            console.log("abc", token);


            res.json({
                code: 200,
                message: "success",
                data: admin,
                token: token
            });}
            else{
                res.json({
                    code: 401,
                    message: "Password not correct"
                });
            }
        })
       
        }
    
    });
    
}
function product(req, res) {
    console.log('req.body-->', req.body)
    var timestamp = Date.now();
    varOrignalImageName = timestamp + "_" + req.files.file[0].orignalname;
    var imagePath = "../28-october-backup-backend-swagger/public/product" + varOrignalImageName;
    fs.writeFile(imagePath, (req.files.file[0].buffer), function (err) {
        if (err) { throw err }
        console.log("img upload")
    })
    var record = new productModel();
    record.pname = req.swagger.params.pname.value;
    record.pdescription = req.swagger.params.pdescription.value;
    record.pcost = req.swagger.params.pcost.value;
    record.status = req.swagger.params.status.value;
    record.file = "http://localhost:3000/product" + varOrignalImageName;
    record.save(function (err, data) {
        console.log('Item listing')
        res.send({ code: 200, message: ' itemlisting', data: data });
    }
    )
}
function listing(req, res) {
    productModel.find(function (err, data) {
        res.json(data);
    })
};
function adduser(req, res) {
    console.log('req.body-->', req.body)
    var obj = {
        name: req.body.name,
        email: req.body.email,
        password: req.body.password,
        role: req.body.role
    };
    var record = new adduserlist(obj);
    record.save(function (err, data) {
        console.log(data);
        if (err) {
            res.json({ code: 400, data: {} });
        } else {
            res.json({ code: 200, data: data });
        }
    })
}
function adduserlisting(req, res) {
    adduserlist.find(function (err, data) {
        res.json(data);
    })
};

function deleteitem(req, res) {
    var id = req.swagger.params.id.value;
    productModel.findByIdAndRemove({ _id: id }, function (err, data) {
        if (err) { throw err; }
        else { res.json(data) }
    })

}
function deleteuserlist(req, res) {
    var id = req.swagger.params.id.value;
    adduserlist.findByIdAndRemove({ _id: id }, function (err, data) {
        if (err) { throw err; }
        else { res.json(data) }
    })

}
function editlistitem(req, res) {
    console.log("in editlist", req.swagger.params.id);
    console.log("in editlist", req.swagger.params.value)
    console.log("query", req.query)
    console.log("body", req.body)

    var _id = req.swagger.params.id.value;
    var timestamp = Date.now();
    varOrignalImageName = timestamp + "_" + req.files.file[0].orignalname;
    var imagePath = "../28-october-backup-backend-swagger/public/product" + varOrignalImageName;
    fs.writeFile(imagePath, (req.files.file[0].buffer), function (err) {
        if (err) { throw err }
        console.log("img upload")
    })
    console.log(_id)

    var pname = req.body.pname;
    var pdescription = req.body.pdescription;
    var pcost = req.body.pcost;
    var status = req.body.status;
    var file = "http://localhost:3000/product" + varOrignalImageName;


    productModel.findByIdAndUpdate(_id, {
        $set: {
            pname: pname,
            pdescription: pdescription,
            pcost: pcost,
            status: status,
            file: file
        }
    }, function (err, data) {
        if (err) { throw err; }
        else { console.log("data:", data) }
        data.save();
        res.json({ data: data, code: 200 })
    });

}
function edituserlist(req,res){
    console.log("in editlist",req.swagger.params.id);
    console.log("in editlist",req.swagger.params.value)
     console.log("query",req.query)
     console.log("body",req.body)
 
     var _id = req.swagger.params.id.value;
     console.log(_id)
 
     var role =req.body.role;

     addusers.findByIdAndUpdate(_id,{$set : {
         role:role,
     }}, function (err, data) {
          if (err) {throw err;}
             else {console.log("data:",data)} 
             data.save();
             res.json({data:data,code:200})
     });
        
 }
 
function addblog(req, res) {
    console.log('req.body-->', req.body)
    var timestamp = Date.now();
    varOrignalImageName = timestamp + "_" + req.files.file[0].orignalname;
    var imagePath = "../28-october-backup-backend-swagger/public/image" + varOrignalImageName;
    fs.writeFile(imagePath, (req.files.file[0].buffer), function (err) {
        if (err) { throw err }
        console.log("img")
    })
    var record = new addblogs();
    record.name = req.swagger.params.name.value;
    record.description = req.swagger.params.description.value;
    record.file = "http://localhost:3000/image" + varOrignalImageName;
    record.save(function (err, data) {
        console.log('blog')
        res.send({ code: 200, message: 'blog add', data: data });
    }
    )
    //     var obj={
    //    name: req.body.name,
    //    description: req.body.description,
    //     };
    //     var record = new addblogs(obj);
    //     record.save(function(err,data){
    //         console.log(data);
    //         if(err){
    //             res.json({code: 400,data:{}});
    //         }else{
    //             res.json({code: 200,data:data});
    //         }
    //       })
}
function bloglisting(req, res) {
    addblogs.find(function (err, data) {
        res.json(data);
    })
};
function deletebloglist(req, res) {
    var id = req.swagger.params.id.value;
    addblogs.findByIdAndRemove({ _id: id }, function (err, data) {
        if (err) { throw err; }
        else { res.json(data) }
    })
}
function addblogcomment(req, res) {
    let user = new blogcomments({
        comment: req.body.comment,
        blogId: req.body.blogId,
        userId: req.body.userId,
        rating: req.body.rating


    })
    user.save(user, function (err, response) {
        if (err) {
            res.json({
                code: 404,
                message: 'admin not Added'
            })
        } else {
            res.json({
                code: 200,
                data: response
            })
            console.log('Successfully created');

        }

    })
}
function blogcommentlisting(req, res) {
    console.log(req.swagger.params.id.value)
    blogcomments.find({ blogId: req.swagger.params.id.value })
        .populate({
            path: 'userId',
            model: 'adduser'
        })
        .exec(function (err, data) {
            if (err) {
                console.log(err)

            } else {
                // console(data)
                res.json(data);
            

            }
        })
};
function getBlogBYid(req, res) {
    var _id = req.swagger.params.id.value;
    addblogs.findOne({ _id: _id }, function (err, data) {
        if (err) {
            console.log(err);
        } else if (data) {
            res.json(data);
        } else {
            console.log("gya kaam se")
        }

    })

}
function getProductBYid(req, res) {
    var _id = req.swagger.params.id.value;
    productModel.findOne({ _id: _id }, function (err, data) {
        if (err) {
            console.log(err);
        } else if (data) {
            res.json(data);
        } else {
            console.log("gya kaam se")
        }

    })

}
function getRoleBYid(req, res) {
    var _id = req.swagger.params.id.value;
    addusers.findOne({ _id: _id }, function (err, data) {
        if (err) {
            console.log(err);
        } else if (data) {
            res.json(data);
        } else {
            console.log("gya kaam se")
        }

    })

}

function addtocart(req,res) {
    var cart=new productcartModel();
    console.log("request",req.body);
    cart.userId=req.body.userId;
    cart.productid=req.body.productid;
    cart.pname=req.body.pname;
    cart.pcost=req.body.pcost;
    // cart.date=req.body.date;  
    cart.quantity=1;

    productcartModel.findOne({
        productid:req.body.productid  
    },function(err,result) {
        if(err) {
            console.log(err);
        } else if(result) {
            console.log(result);
            var quant = result.quantity + 1
            var totalprice;
            totalprice = parseInt(quant) * cart.pcost;
            console.log(quant, " aihsas", totalprice)
            result.quantity = quant
            console.log( result.quantity)
           productcartModel.findOneAndUpdate({productid:req.body.productid},{$set:{
               pcost:totalprice,
               quantity:quant
           }},function(err,data){
               if(err){
                   console.log(err);

               }else{
                   res.json({
                       code: 200,
                       data:data
                   })

               }
           })
           

        }else {
            cart.save(function (err,response) {
                if(err) {
                    res.json({
                        code:404,
                        message:"product not added"
                    })
                } else {
                    res.json({
                        code:200,
                        data:response
                    })
                }
            })
        }
    }
)
}

function listAddToCart(req, res) {
    console.log('swagger res------', res.swagger)
    console.log(req.swagger.params.id.value);

    productcartModel.find({ userId: req.swagger.params.id.value }).sort({ date: 'descending' }).populate({
        
        path: 'productid',
        model: 'adminlisitng',

    }).exec(function (err, data) {
        console.log("After Listing data", data);
        res.json(data);
        if(code=200){
            let userpay =0;
            for(let i=0;i<data.length;i++)
            {
                console.log(userpay + 'and' +data[i].pcost )
                userpay=userpay+data[i].pcost
                console.log('userpay======',userpay)
                
            }
        }
    })

}
function deletecartproduct(req, res) {
    var _id = req.swagger.params.id.value;
    productcartModel.findByIdAndRemove(_id).exec((err, result) => {
        console.log(result);
        res.json({ success: true, code: 200 });
    })

};
function shippingaddress(req, res) {
    console.log('req.body-->', req.body)
    var obj = {
        userId: req.body.userId,
        name: req.body.name,
        address1: req.body.address1,
        address2: req.body.address2,
        contact: req.body.contact,
        city: req.body.city,
        pincode:req.body.pincode,
    };
    var record = new shippingModel(obj);
    record.save(function (err, data) {
        console.log(data);
        if (err) {
            res.json({ code: 400, data: {} });
        } else {
            res.json({ code: 200, data: data });
        }
    })
}
///stripe payement gateway
function generateCardToken(request,res) {
var stripe = require("stripe")("sk_test_g2A5gTTbg4lVZIzPzpeV4ZVg");
const token = request.body.token; 
const amount=request.body.amount;
const LoggedInUser =request.body.LoggedInUser;
var email
var amt=amount*100;
const charge = stripe.charges.create({
  amount: amt,
  LoggedInUser: email,
  currency:'inr',
  source: token,
});
var data={
    message:"success",
    charge:charge
}
// res.send({
//     msg:"success",
//     data: request.body
// })
res.json(data)
}
 
function paymentsucess(req, res) {
    var userId = req.swagger.params.id.value;
    console.log('payment_succes ',userId)
    productcartModel.remove({userId:userId}).exec((err, data) => {
        console.log(data);
        if(err) {throw err}
        else{res.json(data)}
    })

};



