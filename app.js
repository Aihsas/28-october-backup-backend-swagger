'use strict';

var SwaggerExpress = require('swagger-express-mw');
var app = require('express')();
var mongoose = require('mongoose');
var express =require('express');
module.exports = app; // for testing
var express=require('express');
var logger = require('morgan');
var bodyParser = require('body-parser')
var path=require('path');
//chat
var chat = require('./routes/chat');
app.set('view engine', 'html');
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({'extended':'false'}));
app.use(express.static(path.join(__dirname, 'dist')));
app.use('/chat', chat);

var config = {
  appRoot: __dirname // required config
};
//DB Connection

mongoose.Promise = require('bluebird');
mongoose.connect('mongodb://localhost:27017/market');

mongoose.set('debug', true);
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function (callback) {
    console.log('Database connection successful!');
});

app.use(express.static('public'));
SwaggerExpress.create(config, function(err, swaggerExpress) {
  if (err) { throw err; }

  // install middleware
  swaggerExpress.register(app);
  // config swagger ui

  app.use(swaggerExpress.runner.swaggerTools.swaggerUi());

  var port = process.env.PORT || 3000;
  app.listen(port);

  if (swaggerExpress.runner.swagger.paths['/hello']) {
    console.log('try this:\ncurl http://127.0.0.1:' + port + '/hello?name=Scott');
  }
});
